cmake_minimum_required(VERSION 3.15)
project(noughts_crosses C)

set(CMAKE_C_STANDARD 99)
set(LINK_OPTIONS "LINKER:-lwayland-client,-lwayland-egl,-lEGL,-lGLESv2,-ldl,-lfreeimage,-ldbus-1,-lpthread")

include_directories(SYSTEM /usr/include/dbus-1.0 /usr/lib/dbus-1.0/include)

add_executable(noughts_crosses src/xdg_shell.c src/gl.c src/wl_helper.c src/ipc.c src/util.c  main.c)
target_link_options(noughts_crosses PUBLIC ${LINK_OPTIONS})