# Noughts and crosses
Noughts and crosses example (there is no winning condition check yet) written for uni assignment.
Requires **wayland**, **opengl es 3** and **dbus**.
## Build
`mkdir build && cd build && cmake .. && make`

Alternatively, open it as a project in IDE that integrates with CMake (e.g. CLion) and built it there.

You'll also need to build [imagelib](https://gitlab.com/mkls/imagelib) and put resulting .so into directory with
the noughts and crosses binary. Also put images required in config (see main.c, config interpeter part has yet to be rewritten and is hidden into another private repo, huh), default is *kappas.png* and *crosses.jpg*

## Running
`cd path_to_bin_directory && ./noughts_crosses`

It does not require additional arguments but uses stdout/stderr at the moment,
so if you run it by clicking the binary file you won't see any warnings and error messages.