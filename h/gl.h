#ifndef GL_H
#define GL_H

#include "globals.h"
#include <GLES3/gl3.h>

void init_egl(struct client *client);
void draw_window(struct client* wl_client, GLfloat delta);
void draw_net(struct client* wl_client);
void generate_buffers(struct client* wl_client);
void init_gl_stuff(struct client *wl_client);
void draw_textures(struct client *wl_client);

GLboolean check_shader(GLuint shader, char *name);
GLboolean check_link_status(GLuint program);
GLuint compile_shader(const char * const * shader_source, char *name, GLenum type);
GLuint create_shader_program(GLuint vs, GLuint fs);

#endif //GL_H
