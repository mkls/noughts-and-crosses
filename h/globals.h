#ifndef GLOBALS_H
#define GLOBALS_H

#include "../h/xdg_shell.h"

//#include <wayland-client.h>
#include <wayland-egl.h>
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include <dbus/dbus.h>
#include <pthread.h>

struct color {
    float red;
    float green;
    float blue;
    float alpha;
};

struct app_config {
    uint32_t cells;
    uint32_t width;
    uint32_t height;
    struct color bg_color;
    struct color line_color;
    char *cross_icon;
    char *nought_icon;
};

struct client {
    struct wl_display *display;
    struct wl_compositor *compositor;
    struct wl_registry *registry;
    struct wl_surface *surface;
    struct xdg_wm_base *xdg_base;
    struct xdg_surface *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;
    struct wl_egl_window *egl_window;
    struct wl_region *region;
    struct wl_seat *seat;
    struct wl_pointer *pointer;
    struct wl_keyboard *keyboard;

    struct wl_registry_listener *registry_listener;
    struct wl_seat_listener *seat_listener;
    struct wl_pointer_listener *pointer_listener;
    struct wl_keyboard_listener *keyboard_listener;
    struct xdg_wm_base_listener *xdg_wm_base_listener;
    struct xdg_surface_listener *xdg_surface_listener;
    struct xdg_toplevel_listener *xdg_toplevel_listener;

    EGLDisplay egl_display;
    EGLConfig egl_config;
    EGLContext egl_context;
    EGLSurface egl_surface;

    GLfloat *vertices;
    GLuint *net_lines_indices;
    GLuint *noughts_indices;
    GLuint *crosses_indices;
    GLuint *bg_rect_indices;

    GLuint vertex_buffer_id;
    GLuint vertex_array_id;

    GLuint noughts_texture_id;
    GLuint crosses_texture_id;

    GLuint line_program, line_vs, line_fs;
    GLuint point_program, point_vs, point_fs;
    GLuint texture_program, texture_vs, texture_fs;

    uint32_t cursor_x, cursor_y;
    uint32_t cell_x, cell_y;
    uint32_t modifiers;
    uint32_t number_of_vertices, number_of_lines, number_of_faces, number_of_crosses, number_of_noughts;
    char running, updated, use_primitives, resized;
    char side, my_turn;

    char **matrix;

    struct app_config *config;

    char *(*read_image)(char *filename, unsigned *width, unsigned *height);
    void (*free_image)(char *raw_bits);

    DBusConnection *connection;
    char *interface_name;
    char *opponent_name;

    char thread_paused;

    pthread_mutex_t resource_mutex;
};

#endif //GLOBALS_H
