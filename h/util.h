#ifndef UTIL_H
#define UTIL_H

#include "globals.h"

void gen_vertices(struct client *wl_client);
void print_vertices(struct client* wl_client);
void generate_net_indices(struct client* wl_client);
void print_net_indices(struct client* wl_client);
size_t find_next_empty(const GLuint *array, size_t size, size_t step);
char **gen_matrix(int n);
void get_cell_pos(uint32_t x_click, uint32_t y_click, struct client *wl_client);
void add_to_indices(char type, struct client* wl_client);
void alloc_tex_indices(struct client* wl_client);

#endif //UTIL_H
