#ifndef WL_HELPER_H
#define WL_HELPER_H

#include <wayland-client.h>
#include <wayland-egl.h>

void init_wayland(struct client *client);

void pointer_enter(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t sx, wl_fixed_t sy);
void pointer_leave(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface);
void pointer_motion(void *data, struct wl_pointer *pointer, uint32_t time, wl_fixed_t sx, wl_fixed_t sy);
void pointer_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state);
void pointer_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value);
void keyboard_keymap(void *data,
                   struct wl_keyboard *wl_keyboard,
                   uint32_t format,
                   int32_t fd,
                   uint32_t size);
void keyboard_enter(void *data,
                  struct wl_keyboard *wl_keyboard,
                  uint32_t serial,
                  struct wl_surface *surface,
                  struct wl_array *keys);
void keyboard_modifiers(void *data,
                      struct wl_keyboard *wl_keyboard,
                      uint32_t serial,
                      uint32_t mods_depressed,
                      uint32_t mods_latched,
                      uint32_t mods_locked,
                      uint32_t group);
void keyboard_leave(void *data,
                  struct wl_keyboard *wl_keyboard,
                  uint32_t serial,
                  struct wl_surface *surface);
void keyboard_key(void *data,
                struct wl_keyboard *wl_keyboard,
                uint32_t serial,
                uint32_t time,
                uint32_t key,
                uint32_t state);

void global_add(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version);
void global_remove(void *data, struct wl_registry *registry, uint32_t name);

void ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial);
void xdg_surface_configure(void *data, struct xdg_surface *xdg_surface, uint32_t serial);

void xdg_toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height, struct wl_array *states);
void xdg_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel);

void handle_popup_done(void *data, struct wl_shell_surface *wl_shell_surface);

void seat_handle_capabilities(void *data, struct wl_seat *seat, enum wl_seat_capability caps);
void seat_name(void *data, struct wl_seat *wl_seat, const char *name);

struct client* init_wl_client(struct app_config *config);
void destroy_client(struct client* wl_client);

#endif //WL_HELPER_H
