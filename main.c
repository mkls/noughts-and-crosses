#include "h/globals.h"
#include "h/wl_helper.h"
#include "h/gl.h"
#include "h/ipc.h"

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

//TODO: rewrite config read module

struct app_config config = {
        .width = 500,
        .height = 400,
        .bg_color = (struct color){0.0f, 0.5f, 0.8f, 1.0f},
        .line_color = (struct color){1.0f, 1.0f, 1.0f, 1.0f},
        .cells = 3,
        .cross_icon = "kappas.png",
        .nought_icon = "crosses.jpg",
};

void gl_draw(struct client *wl_client) {
     eglMakeCurrent(wl_client->egl_display, wl_client->egl_surface, wl_client->egl_surface, wl_client->egl_context);
     //eglSwapInterval(wl_client->egl_display, 1);

     GLfloat delta = 0.01;
     while (wl_client->running) {

         if (wl_client->resized) {
             wl_egl_window_resize(wl_client->egl_window, wl_client->config->width, wl_client->config->height, 0, 0);
             glViewport(0, 0, wl_client->config->width, wl_client->config->height);
         }

         pthread_mutex_lock(&(wl_client->resource_mutex));
         if (!wl_client->thread_paused)
            draw_window(wl_client, delta);
         draw_textures(wl_client);
         draw_net(wl_client);

         if (wl_client->config->bg_color.blue >= 1.0 || wl_client->config->bg_color.blue <= 0.0) {
             delta *= -1;
         }

         pthread_mutex_unlock(&(wl_client->resource_mutex));
         eglSwapBuffers(wl_client->egl_display, wl_client->egl_surface);
         //usleep(16700);
    }
}

int main() {
    pthread_t gl_thread;
    struct client* wl_client = init_wl_client(&config);

    init_dbus_connect(wl_client);
    init_wayland(wl_client);
    init_egl(wl_client);
    init_gl_stuff(wl_client);

    if (wl_client->running == 0)
        puts("Waiting for the second player.\n");


    while (wl_client->running == 0) {
        dbus_connection_read_write_dispatch(wl_client->connection, 100);
    }

    eglMakeCurrent(wl_client->egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    pthread_create(&gl_thread, NULL, (void*)gl_draw, wl_client);

    while (wl_client->running && wl_display_dispatch(wl_client->display) != -1) {
        dbus_connection_read_write_dispatch(wl_client->connection, 0);
    }

    pthread_join(gl_thread, NULL);

    destroy_client(wl_client);

    return 0;
}