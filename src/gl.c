#include "../h/gl.h"
#include "../h/globals.h"

#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include <stdio.h>
#include <stdlib.h>


void init_egl(struct client *client) {
    EGLint major, minor, configs_number, n;
    EGLConfig *configs;

    const EGLint context_attributes[] = {
            EGL_CONTEXT_MAJOR_VERSION, 3,
            EGL_NONE
    };

    EGLint config_attributes[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RED_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_BLUE_SIZE, 8,
            EGL_DEPTH_SIZE, 24,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT, EGL_NONE
    };

    client->egl_display = eglGetDisplay((EGLNativeDisplayType) client->display);
    if (!eglBindAPI(EGL_OPENGL_ES_API)) {
        printf("failed to bind EGL client API\n");
    }

    if (!eglInitialize(client->egl_display, &major, &minor)) {
        fprintf(stderr, "eglInitilize failed\n");
        exit(EXIT_FAILURE);
    }
    printf("EGL version is %d.%d\n", major, minor);
    eglGetConfigs(client->egl_display, NULL, 0, &configs_number);
    if (configs_number == 0) {
        fprintf(stderr, "Failed to retrieve egl configs\n");
        exit(EXIT_FAILURE);
    }
    configs = calloc(configs_number, sizeof *configs);

    eglChooseConfig(client->egl_display, config_attributes, configs, configs_number, &n);
    client->egl_config = configs[0];
    free(configs);

    client->egl_window = wl_egl_window_create(client->surface, client->config->width, client->config->height);
    client->egl_surface = eglCreateWindowSurface(client->egl_display, client->egl_config, client->egl_window, NULL);
    client->egl_context = eglCreateContext(client->egl_display, client->egl_config, EGL_NO_CONTEXT, context_attributes);

    if (!eglMakeCurrent(client->egl_display, client->egl_surface, client->egl_surface, client->egl_context)) {
        fprintf(stderr, "Failed to make egl context current\n");
        printf("error code is %d\n", eglGetError());
    }
}

void draw_window(struct client* wl_client, GLfloat delta) {
    glClear(GL_COLOR_BUFFER_BIT);
    wl_client->config->bg_color.blue += delta;
    glClearColor(wl_client->config->bg_color.red,
                 wl_client->config->bg_color.green,
                 wl_client->config->bg_color.blue,
                 wl_client->config->bg_color.alpha);
    //eglSwapBuffers(wl_client->egl_display, wl_client->egl_surface);
}

GLboolean check_shader(GLuint shader, char *name) {
    GLint status;

    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        printf("%s shader compilation failed\n", name);
        GLint logSize = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
        char *log = malloc(sizeof(char) * logSize);
        glGetShaderInfoLog(shader, logSize, NULL, log);
        if (logSize > 0) {
            printf("%s\n", log);
        }
    }

    return status;
}

GLboolean check_link_status(GLuint program) {
    GLint linked;

    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE) {
        printf("GL program link failed\n");
        GLint log_length = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_length);
        char *link_log = malloc(sizeof(char) * log_length);
        glGetProgramInfoLog(program, log_length, NULL, link_log);
        if (log_length > 0) printf("%s\n", link_log);
    }
}

GLuint compile_shader(const char * const * shader_source, char *name, GLenum type) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, shader_source, NULL);
    glCompileShader(shader);

    check_shader(shader, name);

    return shader;
}

GLuint create_shader_program(GLuint vs, GLuint fs) {
    GLuint program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);

    check_link_status(program);

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}

void generate_buffers(struct client* wl_client) {
    glGenBuffers(1, &(wl_client->vertex_buffer_id));
    glBindBuffer(GL_ARRAY_BUFFER, wl_client->vertex_buffer_id);

    glBufferData(GL_ARRAY_BUFFER,
                 wl_client->number_of_vertices * 4 * sizeof(GLfloat),
                 wl_client->vertices,
                 GL_STATIC_DRAW);

    glGenVertexArrays(1, &(wl_client->vertex_array_id));
    glBindVertexArray(wl_client->vertex_array_id);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);

    if (glGetError() == GL_NO_ERROR) {
        printf("Successfully generated buffers\n");
    }
}

void draw_net(struct client* wl_client) {
    glUseProgram(wl_client->line_program);
    glLineWidth(4.f);
    glVertexAttrib4f(1,
                     wl_client->config->line_color.red,
                     wl_client->config->line_color.green,
                     wl_client->config->line_color.blue,
                     wl_client->config->line_color.alpha);
    glDrawElements(GL_LINES, wl_client->number_of_lines * 2, GL_UNSIGNED_INT, wl_client->net_lines_indices);
}

void generate_textures(struct client *wl_client) {
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, 4 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));

    glGenTextures(1, &(wl_client->noughts_texture_id));
    glGenTextures(1, &(wl_client->crosses_texture_id));

    unsigned nought_icon_width, nought_icon_height,
             cross_icon_width,  cross_icon_height;
    char *nought_bits, *cross_bits;
    nought_bits = wl_client->read_image(wl_client->config->nought_icon, &nought_icon_width, &nought_icon_height);
    cross_bits = wl_client->read_image(wl_client->config->cross_icon, &cross_icon_width, &cross_icon_height);
    if (nought_bits == NULL || cross_bits == NULL) {
        fprintf(stderr, "Failed to generate textures\n");
        // TODO: fall back to primitive drawing
        exit(EXIT_FAILURE);
    }

    // Noughts
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, wl_client->noughts_texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA8,
                 nought_icon_width,
                 nought_icon_height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 nought_bits);

    // Crosses
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, wl_client->crosses_texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA8,
                 cross_icon_width,
                 cross_icon_height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 cross_bits);

    wl_client->free_image(nought_bits);
    wl_client->free_image(cross_bits);
}

void draw_textures(struct client *wl_client) {
    glUseProgram(wl_client->texture_program);

    GLint s_texture_coord;

    glActiveTexture(GL_TEXTURE0);
    s_texture_coord = glGetUniformLocation(wl_client->texture_program, "s_texture");
    glUniform1i(s_texture_coord, 0);
    glDrawElements(GL_TRIANGLES, wl_client->number_of_noughts * 6, GL_UNSIGNED_INT, wl_client->noughts_indices);

    glActiveTexture(GL_TEXTURE1);
    s_texture_coord = glGetUniformLocation(wl_client->texture_program, "s_texture");
    glUniform1i(s_texture_coord, 1);
    glDrawElements(GL_TRIANGLES, wl_client->number_of_crosses * 6, GL_UNSIGNED_INT, wl_client->crosses_indices);
}

void init_gl_stuff(struct client *wl_client) {
    generate_buffers(wl_client);

    // TODO: fix this shader mess
    const char *line_vertex_shader =
            "#version 300 es\n"
            "layout(location = 1) in vec4 line_color;\n"
            "in vec2 vp;\n"
            "out vec4 vertex_color;\n"
            "void main() {\n"
            "  gl_Position = vec4(vp, 0.0, 1.0);\n"
            "  vertex_color = line_color;\n"
            "}\n";

    const char *line_fragment_shader =
            "#version 300 es\n"
            "precision mediump float;\n"
            "in vec4 vertex_color;\n"
            "out vec4 frag_colour;\n"
            "void main() {\n"
            "  frag_colour = vertex_color;\n"
            "}\n";

    const char* circle_vertex_shader =
            "#version 300 es\n"
            "uniform float size;"
            "in vec2 vp;"
            "void main() {"
            "  gl_PointSize = size;"
            "  gl_Position = vec4(vp, 0.0, 1.0);"
            "}";

    const char* circle_fragment_shader =
            "#version 300 es\n"
            "precision mediump float;"
            "out vec4 frag_colour;"
            "void main() {"
            "  if (length(gl_PointCoord - vec2(0.5)) > 0.5 || length(gl_PointCoord - vec2(0.5)) < 0.35)"
            "    discard;"
            "  frag_colour = vec4(0.0, 1.0, 0.5, 1.0);"
            "}";

    const char* texture_vertex_shader =
            "#version 300 es\n"
            "layout(location = 0) in vec2 a_position;"
            "layout(location = 2) in vec2 a_texCoord;"
            "out vec2 v_texCoord;"
            "void main()"
            "{"
            "    gl_Position = vec4(a_position, 0.0, 1.0);"
            "    v_texCoord = a_texCoord;"
            "}";

    const char* texture_fragment_shader =
            "#version 300 es\n"
            "precision mediump float;"
            "in vec2 v_texCoord;"
            "out vec4 outColor;"
            "uniform sampler2D s_texture;"
            //"varying vec2 v_texCoord;"
            "void main()"
            "{"
            "    outColor = texture(s_texture, v_texCoord);"
            "}";

    // TODO: Add background gradient
    /*
    const char* background_vertex_shader =
            "#version 300 es\n"
            "in vec2 pos;"
            "out vec4"
    const char* background_fragment_shader =
            "#version 300 es\n"
            "precision mediump float;"
            "in vec4 color;"
    */

    wl_client->line_vs = compile_shader(&line_vertex_shader, "Line vertex shader", GL_VERTEX_SHADER);
    wl_client->line_fs = compile_shader(&line_fragment_shader, "Line fragment shader", GL_FRAGMENT_SHADER);
    wl_client->line_program = create_shader_program(wl_client->line_vs, wl_client->line_fs);

    if (wl_client->use_primitives == 1) {
        wl_client->point_vs = compile_shader(&circle_vertex_shader, "Circle vertex shader", GL_VERTEX_SHADER);
        wl_client->point_fs = compile_shader(&circle_fragment_shader, "Circle fragment shader", GL_FRAGMENT_SHADER);
        wl_client->point_program = create_shader_program(wl_client->point_vs, wl_client->point_fs);
    } else {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        generate_textures(wl_client);
        wl_client->texture_vs = compile_shader(&texture_vertex_shader, "Texture vertex shader", GL_VERTEX_SHADER);
        wl_client->texture_fs = compile_shader(&texture_fragment_shader, "Texture fragment shader", GL_FRAGMENT_SHADER);
        wl_client->texture_program = create_shader_program(wl_client->texture_vs, wl_client->texture_fs);
    }
}