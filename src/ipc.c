#include "../h/ipc.h"
#include "../h/util.h"

#include <dbus/dbus.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void check_and_abort(DBusError *error) {
    if (dbus_error_is_set(error)) {
        puts(error->message);
        abort();
    }
}

void respond_to_introspect(DBusConnection *connection, DBusMessage *request, struct client* wl_client) {
    DBusMessage *reply;

    char *buf = malloc(sizeof(char) * 650);
    //char buf[645];
    const char* format_str =
            "<!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\" "
            "\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">"
            "<node>"
            "   <interface name=\"org.freedesktop.DBus.Introspectable\">"
            "       <method name=\"Introspect\">"
            "           <arg name=\"xml_data\" direction=\"out\" type=\"s\"/>"
            "       </method>"
            "   </interface>"
            "   <interface name=\"me.mk.App%u\">"
            "       <method name=\"Start\">"
            "       </method>"
            "       <method name=\"Update\">"
            "           <arg name=\"x_coord\" direction=\"in\" type=\"u\"/>"
            "           <arg name=\"y_coord\" direction=\"in\" type=\"u\"/>"
            "       </method>"
            "   </interface>"
            "</node>";

    sprintf(buf, format_str, wl_client->side + 1);

    reply = dbus_message_new_method_return(request);
    dbus_message_append_args(reply,
                             DBUS_TYPE_STRING, &buf,
                             DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

void respond_to_update(DBusConnection *connection, DBusMessage *message, struct client* wl_client) {
    DBusMessage *reply;
    DBusError error;
    uint32_t x = 0, y = 0;

    dbus_error_init(&error);

    dbus_message_get_args(message, &error,
                          DBUS_TYPE_UINT32, &x,
                          DBUS_TYPE_UINT32, &y,
                          DBUS_TYPE_INVALID);
    if (dbus_error_is_set(&error)) {
        fprintf(stderr, "Wrong arguments to update method\n");
        return;
    }

    pthread_mutex_lock(&(wl_client->resource_mutex));
    wl_client->cell_x = x;
    wl_client->cell_y = y;

    if (wl_client->matrix[x][y] == 'E') {
        if (wl_client->side == 0) {
            wl_client->matrix[x][y] = 'C';
            add_to_indices(1, wl_client);
        } else {
            wl_client->matrix[x][y] = 'N';
            add_to_indices(0, wl_client);
        }

        wl_client->updated = 1;
        wl_client->my_turn = 1;
    } else {
        printf("Cell (%u, %u) is already occupied\n", x, y);
    }
    pthread_mutex_unlock(&(wl_client->resource_mutex));

    reply = dbus_message_new_method_return(message);
    dbus_message_append_args(reply, DBUS_TYPE_INVALID);
    dbus_connection_send(connection, reply, NULL);
    dbus_message_unref(reply);
}

DBusHandlerResult handler(DBusConnection *connection, DBusMessage *message, void *user_data) {
    const char *interface_name = dbus_message_get_interface(message);
    const char *member_name = dbus_message_get_member(message);

    struct client *wl_client = (struct client*)user_data;

    if (strcmp(interface_name, "org.freedesktop.DBus.Introspectable") == 0 &&
        strcmp(member_name, "Introspect") == 0) {
        respond_to_introspect(connection, message, wl_client);
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    /*
    char buf[10];
    sprintf(buf, "me.mk.App%u", wl_client->side + 1);
    */

    if (strcmp(interface_name, wl_client->interface_name) == 0) {
        if (strcmp(member_name, "Start") == 0) {
            wl_client->running = 1;
        } else if (strcmp(member_name, "Update") == 0) {
            respond_to_update(connection, message, wl_client);
        }
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

void init_dbus_connect(struct client* wl_client) {
    DBusError error;
    DBusObjectPathVTable vtable;

    dbus_error_init(&error);
    wl_client->connection = dbus_bus_get(DBUS_BUS_SESSION, &error);
    check_and_abort(&error);

    int ret = dbus_bus_request_name(wl_client->connection, "me.mk.App1", DBUS_NAME_FLAG_REPLACE_EXISTING, &error);
    if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
        ret = dbus_bus_request_name(wl_client->connection, "me.mk.App2", DBUS_NAME_FLAG_REPLACE_EXISTING, &error);
        if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
            fprintf(stderr, "Too many players. Exiting.\n");
            exit(1);
        }

        wl_client->interface_name = "me.mk.App2";
        wl_client->opponent_name = "me.mk.App1";
        wl_client->side = 1;
        wl_client->running = 1;
        wl_client->my_turn = 0;

        DBusMessage *message = dbus_message_new_method_call(wl_client->opponent_name,
                                                            "/App",
                                                            wl_client->opponent_name,
                                                            "Start");
        dbus_connection_send(wl_client->connection, message, NULL);
        dbus_message_unref(message);
    } else {
        wl_client->interface_name = "me.mk.App1";
        wl_client->opponent_name = "me.mk.App2";
        wl_client->side = 0;
    }

    check_and_abort(&error);

    vtable.unregister_function = NULL;
    vtable.message_function = handler;

    dbus_connection_try_register_object_path(wl_client->connection,
                                             "/App",
                                             &vtable,
                                             wl_client,
                                             &error);
    check_and_abort(&error);
}
