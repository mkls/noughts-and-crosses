#include <stdlib.h>
#include <stdio.h>
#include "../h/util.h"

void gen_vertices(struct client *wl_client) {
    // 4 components for each vertex: x, y, s, t
    uint32_t vertex_number = 4 * (wl_client->config->cells + 1) * (wl_client->config->cells + 1);
    wl_client->vertices = (GLfloat *)malloc(sizeof(GLfloat) * vertex_number);

    GLfloat x,       y,       s,       t,
            x_step,  y_step,  s_step,  t_step;

    x = -1.f;
    x_step = 2.f / wl_client->config->cells;

    y = 1.f;
    y_step = 2.f / wl_client->config->cells;

    s = 0.f;
    s_step = 1.f / wl_client->config->cells;

    t = 1.f;
    t_step = 1.f / wl_client->config->cells;

    for (size_t i = 0, j = 0; i < vertex_number; i += 4, j++) {
        wl_client->vertices[i]     = x;
        wl_client->vertices[i + 1] = y;
        wl_client->vertices[i + 2] = s;
        wl_client->vertices[i + 3] = t;

        x += x_step;
        s += s_step;

        if (j == wl_client->config->cells) {
            x = -1.f;
            y -= y_step;
            s = 0.f;
            t -= t_step;
            j = -1;
        }
    }
}

void generate_net_indices(struct client* wl_client) {
    // Each line consists of 2 vertices -> 2 indices
    uint32_t lines_vert_number = (wl_client->config->cells - 1) * 2;

    wl_client->net_lines_indices = (GLuint*)malloc(sizeof(GLuint) * lines_vert_number * 2);

    size_t i = 0;

    // Vertical lines
    for (size_t j = 1; j <= wl_client->config->cells - 1; i += 2, j++) {
        wl_client->net_lines_indices[i] = j;
        wl_client->net_lines_indices[i + 1] = j + wl_client->config->cells * (wl_client->config->cells + 1);
    }

    // Horizontal lines;
    for (size_t j = wl_client->config->cells + 1;
         j <= wl_client->config->cells + 1 + (wl_client->config->cells + 1) * (wl_client->config->cells - 1);
         j += wl_client->config->cells + 1, i += 2) {
        wl_client->net_lines_indices[i]     = j;
        wl_client->net_lines_indices[i + 1] = j + wl_client->config->cells;
    }
}

void print_net_indices(struct client* wl_client) {
    uint32_t lines_index_number = (wl_client->config->cells - 1) * 4;

    printf("Line indices:\n");
    for (size_t i = 0, j = 0; i < lines_index_number; i += 2, j++) {
        printf("(%u, %u)\n", wl_client->net_lines_indices[i], wl_client->net_lines_indices[i + 1]);
    }
}

void print_vertices(struct client* wl_client) {
    size_t s = 4 * (wl_client->config->cells + 1) * (wl_client->config->cells + 1);
    for (size_t i = 0, j = 0; i < s; i += 4, j++) {
        printf("(%f, %f, %f, %f)  ",
                wl_client->vertices[i],
                wl_client->vertices[i + 1],
                wl_client->vertices[i + 2],
                wl_client->vertices[i + 3]);
        if (j == wl_client->config->cells) {
            j = -1;
            printf("\n");
        }
    }
}

void get_cell_pos(uint32_t x_click, uint32_t y_click, struct client *wl_client) {
    wl_client->cell_x = x_click / (wl_client->config->width / wl_client->config->cells);
    wl_client->cell_y = y_click / (wl_client->config->height / wl_client->config->cells);
    //printf("Clicked cell (%d, %d)\n", *c_y, *c_x);
}

char **gen_matrix(int n) {
    char **m = malloc(sizeof(char*) * n);
    for (int i = 0; i < n; i++) {
        m[i] = malloc(sizeof(char) * n);
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            m[i][j] = 'E';
        }
    }

    return m;
}

void alloc_tex_indices(struct client* wl_client) {
    wl_client->noughts_indices = (GLuint*)malloc(sizeof(GLuint) * wl_client->config->cells * wl_client->config->cells * 6);
    wl_client->crosses_indices = (GLuint*)malloc(sizeof(GLuint) * wl_client->config->cells * wl_client->config->cells * 6);

    for (size_t i = 0; i < wl_client->config->cells * wl_client->config->cells * 6; i++) {
        wl_client->noughts_indices[i] = -42;
        wl_client->crosses_indices[i] = -42;
    }
}

size_t find_next_empty(const GLuint *array, size_t size, size_t step) {
    for (size_t i = 0; i < size * step; i += step) {
        if (array[i] == -42) {
            return i;
        }
    }
}

void add_to_indices(char side, struct client* wl_client) {
    uint32_t index;
    if (side == 0) {
        index = find_next_empty(wl_client->noughts_indices, wl_client->config->cells * wl_client->config->cells, 6);

        wl_client->noughts_indices[index]     = wl_client->cell_x + (wl_client->cell_y + 1) * (wl_client->config->cells + 1);
        wl_client->noughts_indices[index + 1] = wl_client->noughts_indices[index] + 1;
        wl_client->noughts_indices[index + 2] = wl_client->cell_x + wl_client->cell_y * (wl_client->config->cells + 1);
        wl_client->noughts_indices[index + 3] = wl_client->noughts_indices[index + 2];
        wl_client->noughts_indices[index + 4] = wl_client->noughts_indices[index + 1];
        wl_client->noughts_indices[index + 5] = wl_client->noughts_indices[index + 2] + 1;

        wl_client->number_of_noughts++;
    } else {
        index = find_next_empty(wl_client->crosses_indices, wl_client->config->cells * wl_client->config->cells, 6);
        wl_client->crosses_indices[index]     = wl_client->cell_x + (wl_client->cell_y + 1) * (wl_client->config->cells + 1);
        wl_client->crosses_indices[index + 1] = wl_client->crosses_indices[index] + 1;
        wl_client->crosses_indices[index + 2] = wl_client->cell_x + wl_client->cell_y * (wl_client->config->cells + 1);
        wl_client->crosses_indices[index + 3] = wl_client->crosses_indices[index + 2];
        wl_client->crosses_indices[index + 4] = wl_client->crosses_indices[index + 1];
        wl_client->crosses_indices[index + 5] = wl_client->crosses_indices[index + 2] + 1;

        wl_client->number_of_crosses++;
    }
}