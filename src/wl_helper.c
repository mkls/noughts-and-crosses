#include "../h/globals.h"
#include "../h/wl_helper.h"
#include "../h/util.h"
#include "../h/gl.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>
#include <dbus/dbus.h>
#include <time.h>
#include <linux/input-event-codes.h>
#include <GLES3/gl3.h>

// Pointer handling
void pointer_enter(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t sx, wl_fixed_t sy) {
    //printf("Pointer entered surface %p at %d %d\n", surface, sx, sy);
}

void pointer_leave(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface) {
    //printf("Pointer left surface %p\n", surface);
}

void pointer_motion(void *data, struct wl_pointer *pointer, uint32_t time, wl_fixed_t sx, wl_fixed_t sy) {
    struct client *wl_client = (struct client*)data;
    wl_client->cursor_x = wl_fixed_to_int(sx);
    wl_client->cursor_y = wl_fixed_to_int(sy);
}

void pointer_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state) {
    struct client* wl_client = (struct client*)data;
    if (button == BTN_LEFT && state == WL_POINTER_BUTTON_STATE_RELEASED) {
        //printf("Clicked the left button at (%d, %d)\n", wl_client->cursor_x, wl_client->cursor_y);

        pthread_mutex_lock(&(wl_client->resource_mutex));
        if (!wl_client->my_turn) {
            printf("Wait for your turn!\n");
            pthread_mutex_unlock(&(wl_client->resource_mutex));
            return;
        }

        get_cell_pos(wl_client->cursor_x, wl_client->cursor_y, wl_client);
        if (wl_client->matrix[wl_client->cell_x][wl_client->cell_y] == 'E') {
            //if (wl_client->side == 0) {
            if (wl_client->side == 0) {
                wl_client->matrix[wl_client->cell_x][wl_client->cell_y] = 'N';
                add_to_indices(0, wl_client);
            } else {
                wl_client->matrix[wl_client->cell_x][wl_client->cell_y] = 'C';
                add_to_indices(1, wl_client);
            }
            wl_client->my_turn = 0;

            DBusMessage *message = dbus_message_new_method_call(wl_client->opponent_name,
                                                                "/App",
                                                                wl_client->opponent_name,
                                                                "Update");
            dbus_message_append_args(message,
                                     DBUS_TYPE_UINT32, &wl_client->cell_x,
                                     DBUS_TYPE_UINT32, &wl_client->cell_y,
                                     DBUS_TYPE_INVALID);
            dbus_connection_send(wl_client->connection, message, NULL);
            dbus_message_unref(message);
        } else {
            printf("Cell (%u, %u) is already occupied\n", wl_client->cell_x, wl_client->cell_y);
        }
        pthread_mutex_unlock(&(wl_client->resource_mutex));
    }
}
void pointer_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value) {
}

void keyboard_keymap(void *data,
                   struct wl_keyboard *wl_keyboard,
                   uint32_t format,
                   int32_t fd,
                   uint32_t size) {
}

void keyboard_enter(void *data,
                  struct wl_keyboard *wl_keyboard,
                  uint32_t serial,
                  struct wl_surface *surface,
                  struct wl_array *keys) {

}

void keyboard_leave(void *data,
                  struct wl_keyboard *wl_keyboard,
                  uint32_t serial,
                  struct wl_surface *surface) {
}

void keyboard_modifiers(void *data,
                      struct wl_keyboard *wl_keyboard,
                      uint32_t serial,
                      uint32_t mods_depressed,
                      uint32_t mods_latched,
                      uint32_t mods_locked,
                      uint32_t group) {
    struct client *wl_client = (struct client*)data;

    pthread_mutex_lock(&(wl_client->resource_mutex));
    wl_client->modifiers = mods_depressed;
    pthread_mutex_unlock(&(wl_client->resource_mutex));
}

void keyboard_key(void *data,
                struct wl_keyboard *wl_keyboard,
                uint32_t serial,
                uint32_t time,
                uint32_t key,
                uint32_t state) {
    struct client *wl_client = (struct client*)data;
    pthread_mutex_lock(&(wl_client->resource_mutex));
    if ((key == KEY_Q && wl_client->modifiers == 4) || key == KEY_ESC) {
        wl_client->running = 0;
        printf("Ctrl+Q or ESC has been pressed, exiting...\n");
    }
    if (key == KEY_ENTER && wl_client->modifiers == 1 && state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        system("lowriter");
    } else if (key == KEY_ENTER && state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        wl_client->config->bg_color.red = (float)rand()/RAND_MAX;
        wl_client->config->bg_color.green = (float)rand()/RAND_MAX;
        wl_client->config->bg_color.blue = (float)rand()/RAND_MAX;
    } else if (key == KEY_SPACE && state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        //printf("wl_client modifiers: %d\n", wl_client->modifiers);
        wl_client->thread_paused = 1;
    } else if (key == KEY_SPACE && state == WL_KEYBOARD_KEY_STATE_RELEASED) {
        wl_client->thread_paused = 0;
    }
    pthread_mutex_unlock(&(wl_client->resource_mutex));
}

void global_add(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version) {
    struct client *wl_client = (struct client*)data;

    if (strcmp(interface, "wl_compositor") == 0) {
        wl_client->compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 1);
    } else if (strcmp(interface, "xdg_wm_base") == 0) {
        wl_client->xdg_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
        xdg_wm_base_add_listener(wl_client->xdg_base, wl_client->xdg_wm_base_listener, data);
    } else if (strcmp(interface, "wl_seat") == 0) {
        wl_client->seat = wl_registry_bind(registry, name, &wl_seat_interface, 1);
        wl_seat_add_listener(wl_client->seat, wl_client->seat_listener, data);
    }
}

void global_remove(void *data, struct wl_registry *registry, uint32_t name) {
    printf("removed\n");
}

void ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
    xdg_wm_base_pong(xdg_wm_base, serial);
}

// Seat listener
void seat_handle_capabilities(void *data, struct wl_seat *seat, enum wl_seat_capability caps) {
    struct client *wl_client = (struct client*)data;

    if (caps & (unsigned)WL_SEAT_CAPABILITY_POINTER) {
        printf("Detected a pointer device.\n");
        wl_client->pointer = wl_seat_get_pointer(seat);
        wl_pointer_add_listener(wl_client->pointer, wl_client->pointer_listener, wl_client);
    }
    if (caps & (unsigned)WL_SEAT_CAPABILITY_KEYBOARD) {
        printf("Detected a keyboard.\n");
        wl_client->keyboard = wl_seat_get_keyboard(seat);
        wl_keyboard_add_listener(wl_client->keyboard, wl_client->keyboard_listener, wl_client);
    }
    if (caps & (unsigned)WL_SEAT_CAPABILITY_TOUCH) {
        printf("Detected a touchscreen.\n");
    }
}

void seat_name(void *data, struct wl_seat *wl_seat, const char *name) {
    printf("Seat %s", name);
}

void init_wayland(struct client *client) {
    client->display = wl_display_connect(NULL);
    if (client->display == NULL) {
        fprintf(stderr, "Error connecting to Wayland display\n");
        exit(EXIT_FAILURE);
    }
    client->registry = wl_display_get_registry(client->display);
    if (client->registry == NULL) {
        fprintf(stderr, "Error retrieving wl_registry\n");
    }

    wl_registry_add_listener(client->registry, client->registry_listener, client);
    wl_display_dispatch(client->display);
    wl_display_roundtrip(client->display);

    client->surface = wl_compositor_create_surface(client->compositor);
    if (client->surface == NULL) {
        fprintf(stderr, "Failed to create surface\n");
        exit(EXIT_FAILURE);
    }

    client->xdg_surface = xdg_wm_base_get_xdg_surface(client->xdg_base, client->surface);
    //client->shell_surface = wl_shell_get_shell_surface(client->shell, client->surface);
    if (client->xdg_surface == NULL) {
        fprintf(stderr, "Failed to get xdg surface\n");
        exit(EXIT_FAILURE);
    }

    xdg_surface_add_listener(client->xdg_surface, client->xdg_surface_listener, client);

    client->xdg_toplevel = xdg_surface_get_toplevel(client->xdg_surface);
    xdg_toplevel_add_listener(client->xdg_toplevel, client->xdg_toplevel_listener, client);

    xdg_toplevel_set_title(client->xdg_toplevel, "Noughts and crosses pre-pre-pre-alpha");

    client->region = wl_compositor_create_region(client->compositor);
    if (client->region == NULL) {
        fprintf(stderr, "Failed to create wl_region\n");
    }
    wl_region_add(client->region, 0, 0, client->config->width, client->config->height);


    wl_surface_commit(client->surface);
    xdg_toplevel_set_min_size(client->xdg_toplevel, 320, 240);
}

struct client* init_wl_client(struct app_config *config) {
    srand(time(NULL));
    struct client *new_client = malloc(sizeof(struct client));

    new_client->config = config;
    new_client->modifiers = 0;

    new_client->number_of_vertices = (config->cells + 1) * (config->cells + 1);
    new_client->number_of_lines = (config->cells - 1) * 2;

    new_client->egl_display = NULL;
    new_client->egl_surface = NULL;
    new_client->egl_window = NULL;
    new_client->egl_context = NULL;
    new_client->egl_config = NULL;

    new_client->display = NULL;
    new_client->compositor = NULL;
    new_client->registry = NULL;
    new_client->xdg_base = NULL;
    new_client->surface = NULL;
    new_client->xdg_surface = NULL;
    new_client->region = NULL;
    new_client->seat = NULL;
    new_client->pointer = NULL;
    new_client->keyboard = NULL;

    new_client->pointer_listener = NULL;
    new_client->keyboard_listener = NULL;
    new_client->registry_listener = NULL;
    new_client->xdg_surface_listener = NULL;
    new_client->xdg_toplevel_listener = NULL;
    new_client->xdg_wm_base_listener = NULL;
    new_client->seat_listener = NULL;

    new_client->cursor_x = new_client->cursor_y = 0;
    new_client->running = 0;
    new_client->updated = 1;
    new_client->thread_paused = 0;
    new_client->resized = 0;
    new_client->my_turn = 1;

    new_client->matrix = gen_matrix(new_client->config->cells);

    void *handle;
    handle = dlopen("./libimage.so", RTLD_LAZY);

    if (handle == NULL) {
        fprintf(stderr, "Failed to load ./libimage.so\n");
        new_client->use_primitives = 1;
    } else {
        fprintf(stderr, "Loaded ./libimage.so\n");

        new_client->read_image = (char *(*)(char*, unsigned*, unsigned*))dlsym(handle, "read_image");
        new_client->free_image = (void (*))(char*)dlsym(handle, "free_image");

        if (new_client->read_image == NULL || new_client->free_image == NULL) {
            fprintf(stderr, "no such symbol\n");
            new_client->use_primitives = 1;
        }
        new_client->use_primitives = 0;
    }

    //new_client->side = 1;
    new_client->number_of_noughts = new_client->number_of_crosses = 0;

    gen_vertices(new_client);
    //print_vertices(new_client);

    generate_net_indices(new_client);
    //print_net_indices(new_client);

    alloc_tex_indices(new_client);

    // 6 vertices for 2 triangles
    new_client->bg_rect_indices = malloc(sizeof(GLuint) * 6);

    new_client->bg_rect_indices[0] = 0;
    new_client->bg_rect_indices[1] = new_client->config->cells * (new_client->config->cells + 1);
    new_client->bg_rect_indices[2] = new_client->bg_rect_indices[1] + new_client->config->cells;
    new_client->bg_rect_indices[3] = new_client->bg_rect_indices[2];
    new_client->bg_rect_indices[4] = new_client->config->cells;
    new_client->bg_rect_indices[5] = new_client->bg_rect_indices[0];

    new_client->registry_listener = malloc(sizeof(struct wl_registry_listener));
    new_client->pointer_listener = malloc(sizeof(struct wl_pointer_listener));
    new_client->keyboard_listener = malloc(sizeof(struct wl_keyboard_listener));
    new_client->seat_listener = malloc(sizeof(struct wl_seat_listener));
    new_client->xdg_wm_base_listener = malloc(sizeof(struct xdg_wm_base_listener));
    new_client->xdg_toplevel_listener = malloc(sizeof(struct xdg_toplevel_listener));
    new_client->xdg_surface_listener = malloc(sizeof(struct xdg_surface_listener));

    *(new_client->registry_listener) = (struct wl_registry_listener){
            .global = global_add,
            .global_remove = global_remove
    };

    *(new_client->seat_listener) = (struct wl_seat_listener){
            .capabilities = seat_handle_capabilities,
            .name = seat_name
    };

    *(new_client->pointer_listener) = (struct wl_pointer_listener){
            .axis = pointer_axis,
            .button = pointer_button,
            .enter = pointer_enter,
            .leave = pointer_leave,
            .motion = pointer_motion,
    };

    *(new_client->keyboard_listener) = (struct wl_keyboard_listener){
            .enter = keyboard_enter,
            .leave = keyboard_leave,
            .key = keyboard_key,
            .keymap = keyboard_keymap,
            .modifiers = keyboard_modifiers,
    };

    *(new_client->xdg_wm_base_listener) = (struct xdg_wm_base_listener){
        .ping = ping
    };

    *(new_client->xdg_surface_listener) = (struct xdg_surface_listener){
        .configure = xdg_surface_configure
    };

    *(new_client->xdg_toplevel_listener) = (struct xdg_toplevel_listener){
        .configure = xdg_toplevel_configure,
        .close = xdg_toplevel_close
    };

    return new_client;
}

void destroy_client(struct client *wl_client) {
    wl_region_destroy(wl_client->region);
    eglMakeCurrent(wl_client->egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroySurface(wl_client->egl_display, wl_client->egl_surface);
    wl_egl_window_destroy(wl_client->egl_window);

    eglDestroyContext(wl_client->egl_display, wl_client->egl_context);
    eglTerminate(wl_client->egl_display);

    xdg_surface_destroy(wl_client->xdg_surface);
    //wl_shell_surface_destroy(wl_client->shell_surface);
    //wl_shell_destroy(wl_client->shell);
    wl_surface_destroy(wl_client->surface);
    xdg_wm_base_destroy(wl_client->xdg_base);
    wl_registry_destroy(wl_client->registry);
    wl_compositor_destroy(wl_client->compositor);
    wl_display_disconnect(wl_client->display);

    //free(wl_client->shell_surface_listener);
    free(wl_client->keyboard_listener);
    free(wl_client->pointer_listener);
    free(wl_client->seat_listener);
    free(wl_client->registry_listener);
    free(wl_client->xdg_toplevel_listener);
    free(wl_client->xdg_surface_listener);
    free(wl_client->xdg_wm_base_listener);

    free(wl_client->vertices);
    free(wl_client->net_lines_indices);
    free(wl_client->noughts_indices);
    free(wl_client->crosses_indices);

    for (size_t i = 0; i < wl_client->config->cells; i++) {
        free(wl_client->matrix[i]);
    }
    free(wl_client->matrix);

    free(wl_client);
}

void xdg_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel) {
    struct client *wl_client = (struct client*)data;
    wl_client->running = 0;
}

void xdg_surface_configure(void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
    xdg_surface_ack_configure(xdg_surface, serial);
}

void xdg_toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height,
                            struct wl_array *states) {
    struct client *wl_client = (struct client*)data;
    pthread_mutex_lock(&(wl_client->resource_mutex));
    if (width > 0)
        wl_client->config->width = width;
    if (height > 0)
        wl_client->config->height = height;

    wl_client->updated = 1;
    wl_client->resized = 1;

    //xdg_surface_set_window_geometry(wl_client->xdg_surface, 0, 0, wl_client->config->width, wl_client->config->height);
    //wl_egl_window_resize(wl_client->egl_window, wl_client->config->width, wl_client->config->height, 0, 0);
    //glViewport(0, 0, wl_client->config->width, wl_client->config->height);
    pthread_mutex_unlock(&(wl_client->resource_mutex));
}
